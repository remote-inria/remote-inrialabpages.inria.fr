# RemI documentation

This is the online documentation for [**remi**](https://gitlab.inria.fr/remote-inria/remi).

It is available here:
[remote-inria.gitlabpages.inria.fr](https://remote-inria.gitlabpages.inria.fr/)

___
This website uses the [Hugo](https://gohugo.io/) framework with the [geek-doc](https://geekdocs.de/)
theme.
