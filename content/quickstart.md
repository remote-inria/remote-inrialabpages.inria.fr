---
title: Quick Start
---

{{< toc >}}

The first thing to do is to initialize the project like so:
```Shell
[me@local-pc:~]$ cd <PATH_TO_MY_PROJECT>
[me@local-pc:project]$ remi init
```

You will be asked to provide:
- **Project name:** A name for your project (can be anything).
- **Inria username:** Your Inria login (with which you login to your desktop).
- **Inria hostname:** The name of your Inria desktop (`alya`, `scorpio`, `andromeda`...)
- **Gricad _(optional)_:** If you want to use the
    [Gricad cluster](https://gricad-doc.univ-grenoble-alpes.fr/) with **remi**.

This process leads to the creation of a folder `.remi` within your project folder.\
It contains a configuration file (`config.yaml`) and an exclude file (`exclude.txt`).


Once configured, you are ready to go !


## File synchronization

One of the main features of remi is syncing.\
More precisely, this tool sends your code on the storage of your workstation (`/local_scratch`).\
This **MUST NOT** be considered as a kind of 'backup'.\
The only purpose is to copy the code in order
to run it. Each time you will `push`, a one-way synchronization will update the remote copy of your
project and delete the remote files that do not exist anymore locally.

I strongly recommend using a versioning tool like `git` to manage your project as **remi** does not
intent to replace this functionality.

You are not expected to modify the remote copy of your code (this is exactly what **remi** is for:
edit locally, run remotely).


### Exclude mechanism

Some folders and files might not be relevant to sync (`remi push`) on the remote desktop.\
You can edit the exclude file (`.remi/exclude.txt`) and add any regex to ignore files or folders
based on regex (as you would do in a `.gitignore` file).\
By default, this file is not empty and contains locations that should not be synced such as
`output/`, `.git/`, `__pycache__/`...

Here are some directories that are excluded by default:
- `.git`
- `output/`
- `notebooks/`
- ...

This is useful for folders that will be filled on the remote such as `output` and `notebooks`. You
might then run `remi pull <FOLDER>` to fetch this remote data back to your local machine.

**Recap sceme:**
```
╭----------------╮                              ╭--------------------------------╮
| Local computer |                              | Remote storage (local scratch) |
╰----------------╯                              ╰--------------------------------╯
 my_code/   ╮                                   ╭  my_code/
 folder_a/  |                                   |  folder_a/
 folder_b/  |                                   |  folder_b/
 file_a     |---------- `remi push` ----------->|  file_a
 file_b     |                                   |  file_b
 file_c     |                                   |  file_c
 file_d     ╯                                   ╰  file_d
 output/     <------- `remi pull output/`-------   output/
 notebooks/                                        notebooks/
 ignored_folder/                                   other_ignored_folder/


ignored folders (listed in `.remi/exclude.txt`):
- output/
- notebooks/
- ignored_folder/
- other_ignored_folder/
```
