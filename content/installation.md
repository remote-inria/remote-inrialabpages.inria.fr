---
title: Installation
---

## Dependencies and prerequisites

First, you need to set up your `ssh` keys to be able to connect to the Inria computers (learn more
[here](https://team.inria.fr/perception/private/howto-team-it-ressources/#howto-network-access-and-ssh-usage)).

You also need to have `rsync` and `python` (>= 3.7) on your system.

## Installation

The installation is done on the **local PC**.

To perform a simple user installation with `pip`, the following command is enough:
```Shell
pip install remote-inria
```

If you want to do modifications to the code, you may `clone` this repo and install the package in
the _editable_ mode (`pip` `-e` option).
```Shell
git clone git@gitlab.inria.fr:remote-inria/remi.git
cd remi
pip install -e .
```
