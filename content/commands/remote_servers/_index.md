---
title: Remote servers
---

Remote servers are applications that run on a remote computer and can be accessed from your local
browser thanks to remi.

Two such servers are supported right now:
- [Jupyter](/commands/remote_servers/jupyter)
- [TensorBoard](/commands/remote_servers/tensorboard)

Other could be quite easily added in the future if needed.

{{< hint info >}}
Please, note that the connection can take a few seconds before working.
Do not hesitate to refresh your browser once the page opens.
{{< /hint >}}

___
## SSH Tunnel

`remi ssh-tunnel [(-p |  --port) PORT] [(-h | --hostname) HOSTNAME]`\
Start a SSH tunnel between your local machine and `hostname` on the port `port`.

**Options:**
- `-p | --port`: The port (local and remote) for the tunnel.\
_Default:_ `9090` (the `tensorboard` port)
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
