---
title: TensorBoard
---

Look at how your remote experiment is doing thanks to TensorBoard.


**Available subcommands:**
{{< toc >}}

## `start` (default)
`remi tensorboard [start] [OPTIONS]`\
Run a TensorBoard server on the remote desktop.\
`start` is the default sub-command (which can thus be run using `remi tensorboard`).

**Options:**
- `-p | --port`: The port (local and remote) for the server.\
_Default:_ `9090`
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
- `-d | --logdir`: Log directory.\
_Default:_ The value of `logdir` in the `tensorboard` section of the config file (`output/` by
default).
- `--browser/--no-browser`: If enabled, automatically open TensorBoard in the local
browser.\
_Default:_ The value of `open_browser` in the `tensorboard` section of the config file.

___
## `stop`
`remi tensorboard stop [OPTIONS]`\
Stop TensorBoard on the remote desktop.

**Options:**
- `-p | --port`: The port (local and remote) for the server.\
_Default:_ `8080`
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
