---
title: Jupyter notebook
---


Use a jupyter notebook server running on the remote desktop from the local PC.

{{< hint warning >}}
**WARNING:** The way synchronization works with remi will delete all modifications made remotely.
You should put your notebooks in the `notebooks/` directory (which is excluded by default) and get
them back on your local machine using `remi pull notebooks/`.
{{< /hint >}}

**Available subcommands:**
{{< toc >}}

## `start` (default)
`remi jupyter [start] [(-p |  --port) PORT] [(-h | --hostname) HOSTNAME] [--browser/--no-browser]`\
Run a jupyter server on the remote desktop.\
`start` is the default sub-command (which can thus be run using `remi jupyter`).

**Options:**
- `-p | --port`: The port (local and remote) for the server.\
_Default:_ `8080`
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
- `--browser/--no-browser`: If enabled, automatically open the jupyter notebook in the local
browser.\
_Default:_ The value of `open_browser` in the `jupyter` section of the config file.

___
## `stop`
`remi jupyter stop [(-p |  --port) PORT] [(-h | --hostname) HOSTNAME]`\
Stop the jupyter server on the remote desktop.

**Options:**
- `-p | --port`: The port (local and remote) for the server.\
_Default:_ `8080`
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
