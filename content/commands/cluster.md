---
title: Cluster
---

Execute the code on the Inria cluster.
Please note that the cluster request (`oarsub` command) and `singularity` container are
configurable from the [config file](/configuration).

**Inria Grenoble cluster architecture**

<img src="/inria_cluster.png" alt="cluster architecture" height=500>

**Available subcommands:**
{{< toc >}}


## `script` (default)
- `remi cluster [script] [OPTIONS]`\
Run a bash script on the cluster.\
This is the default subcommand (and can thus be run using `remi cluster`).\
**Options:**
- `-s | --script`: The path to a bash script to run.\
_Default:_ `script.sh`
- `-h | --host`: The name of a specific cluster node (ex: `gpu3`, `node-6`...).
_Default:_ The `host_id` property in `config.yaml`.
- `-n | --job-name`: A custom name for the cluster job (`oarsub`'s `--name` option).\
_Default:_ The project name
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `--no-push`: Do not attempt to sync project files to the remote location.
- `--no-build`: Do not attempt to (re)-build the singularity container.

**Examples:**
- `remi cluster`: Run `script.sh` on the cluster.
- `remi cluster -s training_script.sh`: Run `training_script.sh` on the cluster.


___
## `command`
`remi cluster command [OPTIONS] COMMAND`\
Run the specified `COMMAND` on the cluster.

**Options:**
- `-h | --host`: The name of a specific cluster node (ex: `gpu3`, `node-6`...).
_Default:_ The `host_id` property in `config.yaml`.
- `-n | --job-name`: A custom name for the cluster job (`oarsub`'s `--name` option).\
_Default:_ The project name
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `--no-push`: Do not attempt to sync project files to the remote location.
- `--no-build`: Do not attempt to (re)-build the singularity container.

**Example:**
- `remi cluster command "./test.sh --number_steps=1000"`: Run the command `./test.sh --number_steps=1000`
on the cluster.


___
## `interactive`
`remi cluster interactive [OPTIONS]`\
Start an interactive session on the cluster. This runs `oarsub` with the `--interactive` flag.

**Options:**
- `-h | --host`: The name of a specific cluster node (ex: `gpu3`, `node-6`...).
_Default:_ The `host_id` property in `config.yaml`.
- `-n | --job-name`: A custom name for the cluster job (`oarsub`'s `--name` option).\
_Default:_ The project name
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `-X | --xforwarding`: Enables X forwarding in SSH.
- `--no-push`: Do not attempt to sync project files to the remote location.

**Example:**
- `remi cluster interactive --no-push`: Start an interactive session on the cluster without pushing
local changes.

___
## `stat`
`remi cluster stat [ -t | --team ]`\
Get some information about running/planned jobs thanks to `oarstat`.\
By default, it only lists your jobs.

**Options:**
- `-t | --team`: If provided, get jobs of the RobotLearn team members.

___
## `connect`
`remi cluster connect OAR_JOB_ID`\
Connect to a running job.

**Example:**
- `remi cluster connect 6267518`

___
## `kill`
`remi cluster kill OAR_JOB_IDS`\
Kill one or multiple running job(s).

**Example:**
- `remi cluster kill 6267518 6267519 6267520`
