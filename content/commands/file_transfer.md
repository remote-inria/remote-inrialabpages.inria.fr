---
title: Transferring and managing files
---

**Available commands:**
{{< toc >}}

## `push`
`remi push [-f | --force]`\
Sync the content of the project directory to the remote desktop.\
If no changes are detected locally, the file sync will not be attempted.\

**Options:**
- `-f | --force`: Run the sync command even if no local changes were detected.

___
## `pull`
`remi pull [-f | --force] [REMOTE_PATH]`\
Sync the content of the provided `REMOTE_PATH` directory from the remote computer to the local one.\
This can be used to sync back experimental output that result from a computation done remotely.\
If no path is specified, `output/` will be used as the default value.

**Options:**
- `-f | --force`: Do not ask for a confirmation before pulling.\
**Use with caution.** (Eventually conflicting local files might be overridden).

___
## `clean`
`remi clean [-f | --force] [REMOTE_PATH]`\
Clean the content of the provided `REMOTE_PATH` directory on the remote location.\
If no directory is specified, `output/` will be used as the default value.

**Options:**
- `-f | --force`: Do not ask for a confirmation before cleaning.\
**Use with caution.**
