---
title: Singularity container
---

Interact with your singularity container(s).

**Available command:**
{{< toc >}}


## `build-container`
`remi build-container [-f | --force]`\
Build the singularity container on the remote desktop.
If no changes are detected locally, the container build will not be attempted.

**Options:**
- `-f | --force`: Run the build command even in if no local changes were detected in the recipe
  (`.def`) and even if the image already exists.
