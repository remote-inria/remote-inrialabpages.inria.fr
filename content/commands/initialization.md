---
title: Initialization
---

**Available commands:**
{{< toc >}}

## `init`
`remi init`\
Initialize the project in the current working directory. Generate the configuration files.

___
## `setup`
`remi setup [(-h | --hostname) HOSTNAME]`\
Set up remote project location:
- Create the necessary remote directories
- Perform the first _push_ (copy local files to the remote destination)

**Options:**
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.

___
## `update`
`remi update`\
Update `remi` to the latest version.
