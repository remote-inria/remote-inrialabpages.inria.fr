---
title: remi commands
---

Here is a description of all the available _cli_ commands.\
They are all supposed to be ran from the project directory on your local machine.
```bash
[me@local-pc:project]$ remi <COMMAND>
```

Every command is performed from the project root directory. Hence, relative paths from there might
be use.

- [Initialization](/commands/initialization) (things to do to configure your project)
- [Transferring and managing files](/commands/file_transfer)
- [Desktop](/commands/desktop) (run code on the workstations)
- [Singularity](/commands/singularity) (manage your singularity container)
- [Cluster](/commands/cluster) (run code on the Inria cluster)
- [Remote servers](/commands/) (TensorBoard, Jupyter...)
