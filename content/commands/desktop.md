---
title: Desktop (execution on Inria desktop)
---

Execute the code on the remote workstation.

**Available subcommands:**
{{< toc >}}


## `script` (default)
- `remi desktop [script] [OPTIONS]`\
Run a bash script on the remote computer.\
This is the default subcommand (and can thus be run using `remi cluster`).\
**Options:**
- `-s | --script`: The path to a bash script to run.\
_Default:_ `script.sh`
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
- `-g | --gpus`: The value for `$CUDA_VISIBLE_DEVICES` (e.g.: '1,2').\
_Default:_ Nothing: the process will see all GPUs of the system.
- `-b | --background`: Run the script in the background (free the ssh session) by running it inside
  a `screen` or `tmux` session (configured in `.remi/config.yaml`).
- `-n | --name`: Manually name the `screen`/`tmux` session. (Only useful when `background` is
  enabled).\
_Default:_ The project name (`project_name` in the config file).
- `-a | --attach`: If running the job in background mode, directly attach to it.
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `-X | --xforwarding`: Enables X forwarding in SSH.
- `--no-push`: Do not attempt to sync project files to the remote location.
- `--no-build`: Do not attempt to (re)-build the singularity container.

**Examples:**
- `remi desktop`: Run `script.sh` on the default remote desktop.
- `remi desktop -h mensa`: Run `script.sh` on `mensa`.
- `remi desktop -s training_script.sh -b`: Run `training_script.sh` on the default remote desktop
in background mode.

___
## `command`
`remi desktop command [OPTIONS] COMMAND`\
Run the specified `COMMAND` on the remote computer.

**Options:**
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
- `-g | --gpus`: The value for `$CUDA_VISIBLE_DEVICES` (e.g.: '1,2').\
_Default:_ Nothing: the process will see all GPUs of the system.
- `-b | --background`: Run the command in the background (free the ssh session) by running it inside
  a `screen` or `tmux` session (configured in `.remi/config.yaml`).
- `-n | --name`: Manually name the `screen`/`tmux` session. (Only useful when `background` is
  enabled).\
_Default:_ The project name (`project_name` in the config file).
- `-a | --attach`: If running the job in `background` mode, directly attach to it.
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `-X | --xforwarding`: Enables X forwarding in SSH.
- `--no-push`: Do not attempt to sync project files to the remote location.
- `--no-build`: Do not attempt to (re)-build the singularity container.

**Examples:**
- `remi desktop command nvidia-smi`: Run the command `nvidia-smi` on the default remote desktop.
- `remi desktop command -h bacchus "python train_net.py"`: Run the command `train_net.py` on `bacchus`.
- `remi desktop command -b "./test.sh --number_steps=1000"`: Run the command `./test.sh --number_steps=1000`
on the default remote desktop in background mode.

___
## `interactive`
`remi desktop interactive [OPTIONS]`\
Start an interactive session on the remote computer.

**Options:**
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
- `-g | --gpus`: The value for `$CUDA_VISIBLE_DEVICES` (e.g.: '1,2').\
_Default:_ Nothing: the process will see all GPUs of the system.
- `-c | --container`: The name/path of the container image (`.sif`) that you want to use.\
_Default:_ The `singularity.output_sif_name` property in `config.yaml`.
- `--no-push`: Do not attempt to sync project files to the remote location.
- `--no-build`: Do not attempt to (re)-build the singularity container.

**Examples:**
- `remi desktop interactive`: Start an interactive session on the default remote desktop.
- `remi desktop -h hydra --no-push`: Start an interactive session on `hydra` without pushing local
changes.

___
## `attach-session`
`remi desktop attach-session [OPTIONS] [SESSION_NAME]`\
Attach to the `screen`/`tmux` running session named `SESSION_NAME` (default is the project name).

**Options:**
- `-h | --hostname`: The hostname of an Inria computer.\
_Default:_ The value of `desktop.hostname` in the config file.
