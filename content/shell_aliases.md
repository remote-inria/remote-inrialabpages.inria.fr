---
title: Shell aliases
---

Here are some recommended shell aliases to be even faster with **remi**.

You can copy-paste this in your `~/.bashrc` and of course adapt it to your needs !


```shell
################
# RemI aliases #
################

# File transferring
alias rp="remi push"
alias rpf="remi push -f"
alias rpl="remi pull"
alias rcl="remi clean"

# Singularity
alias rbc="remi build-container"

# Inria desktop
alias rd="remi desktop"
alias rdc="remi desktop command"
alias rdi="remi desktop interactive"
alias rda="remi desktop attach-session"

# Inria cluster
alias rc="remi cluster"
alias rcc="remi cluster command"
alias rci="remi cluster interactive"
alias rcs="remi cluster stat"
alias rcst="remi cluster stat --team"
alias rck="remi cluster kill"

# Gricad cluster
alias rg="remi gricad"
alias rgp="remi gricad push"
alias rgpl="remi gricad pull"
alias rgc="remi gricad command"
alias rgi="remi gricad interactive"
alias rgs="remi gricad stat"
alias rgch="remi gricad chandler"
alias rgr="remi gricad recap"
alias rgk="remi gricad kill"

# Remote servers
alias rt="remi tensorboard"
alias rj="remi jupyter"
```
